function Card(rank, suit) {
  return {rank: rank, suit: suit};
}

// 3 4 5 6 7 8 9 10 J(11) Q(12) K(13) A(14) 2(15)
function nameCard(card) {
  let result = "";
  switch (card.rank) {
    case 11: result += "J"; break;
    case 12: result += "Q"; break;
    case 13: result += "K"; break;
    case 14: result += "A"; break;
    case 15: result += "2"; break;
    default: result += card.rank;
  }
  switch (card.suit) {
    case 0: result += "\u2666"; break;
    case 1: result += "\u2663"; break;
    case 2: result += "\u2665"; break;
    case 3: result += "\u2660"; break;
  }
  return result;
}
function colorCard(card) {
  return (card.suit === 0 || card.suit === 2)? Symbol.for("red"): Symbol.for("black");
}
function sortCardsByRank(cards) {
  cards.sort(function(a, b) {
    return a.rank - b.rank || a.suit - b.suit;
  });
  return cards;
}
function sortCardsBySuit(cards) {
  cards.sort(function(a, b) {
    return a.suit - b.suit || a.rank - b.rank;
  });
  return cards;
}

function randomCardsGenerator() {
  let fullDeck = [];
  for (let rank = 3; rank < 16; rank++)
    for (let suit = 0; suit < 4; suit++)
      fullDeck.push(Card(rank, suit));

  for (let i = fullDeck.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * fullDeck.length);
    [fullDeck[i], fullDeck[j]] = [fullDeck[j], fullDeck[i]];
  }
  console.log(fullDeck);
  let pileA = fullDeck.slice(0,9);
  console.log(pileA);
  let pileB = fullDeck.slice(9,18);
  return {"A": pileA, "B": pileB};
}
