var hand = randomCardsGenerator();
var turn = "A";
var stack = [];
//var counting = 0;
function _isFinished() {
  return hand.A.length === 0 || hand.B.length === 0;
}
function _nextTurn() {
  return turn === "A" ? "B" : "A";
}
function _toggleTurn(turn) {
  return turn === "A" ? "B" : "A";
}

function _canPlay(move) {
  let current = stack.length === 0 ? null : stack[stack.length - 1];
  if (move == null) return current == null ? false : true;
  if (current == null) {
    if (_isSingle(move) || _isPair(move) || _isThreeOfAKind(move) || _isFourOfAKind(move) || _isStraight(move) || _isFlush(move) || _isFullHouse(move) || _isFourOfAKindExtra(move)) return true;
    else return false;
  }
  if (current.length !== move.length) return false;
  if (_compareSingles(move, current) === 1 || _comparePairs(move, current) === 1 || _compareThreeOfAKinds(move, current) === 1 || _compareFourOfAKinds(move, current) === 1 || _compareFiveCards(move, current) === 1) return true;
  return false;
}

function _play(move) {
  stack.push(move);
  if (move != null) {
    for (let i = 0; i < move.length; i++) {
      let card = move[i];
      hand[turn] = hand[turn].filter(function(item) { return item.rank !== card.rank || item.suit !== card.suit; });
    }
  }
  turn = _nextTurn(turn);
  return true;
}
function _unplay() {
  if (stack.length === 0) return false;
  turn = _nextTurn(turn);
  let move = stack.pop();
  if (move != null) hand[turn] = hand[turn].concat(move);
  return true;
}
function playWithCards() {
  if (hand.A.length === 0 || hand.B.length === 0) return false;
  let cards = document.getElementsByClassName("chosen");
  if (cards.length === 0 || cards.length > 5) return false;
  let move = [];
  for (let i = 0; i < cards.length; i++) {
    move.push(JSON.parse(cards[i].dataset.id));
  }
  sortCardsByRank(move);
  if (_canPlay(move)) {
    _play(move);
    return true;
  }
  return false;
}
function think() {
  if (hand[_nextTurn(turn)].length === 0) {
    return {score: (999 - stack.length) * (_nextTurn(turn) === "A" ? 1 : -1), moves: []};
  }
  let current = stack.length === 0 ? null : stack[stack.length - 1], moves;
  if (current == null) moves = _getSingles(hand[turn]).concat(_getPairs(hand[turn])).concat(_getThreeOfAKinds(hand[turn])).concat(_getFourOfAKinds(hand[turn])).concat(_getStraight(hand[turn])).concat(_getFlush(hand[turn])).concat(_getFullHouse(hand[turn])).concat(_getFourOfAKindsExtra(hand[turn])).concat(_getStraightFlush(hand[turn]));
  else if (current.length === 1) moves = _getSingles(hand[turn]).concat([null]);
  else if (current.length === 2) moves = _getPairs(hand[turn]).concat([null]);
  else if (current.length === 3) moves = _getThreeOfAKinds(hand[turn]).concat([null]);
  else if (current.length === 4) moves = _getFourOfAKinds(hand[turn]).concat([null]);
  else moves = _getStraight(hand[turn]).concat(_getFlush(hand[turn])).concat(_getFullHouse(hand[turn])).concat(_getFourOfAKindsExtra(hand[turn])).concat(_getStraightFlush(hand[turn])).concat([null]);
  let best = null;
  for (let i = 0; i < moves.length; i++) {
    if (_canPlay(moves[i])) {
      _play(moves[i]);
      //counting++;
      var result = think();
      _unplay();
      if (best == null || result.score > best.score && turn === "A" || result.score < best.score && turn === "B") {
        best = result;
        best.moves.push(moves[i]);
      }
      if (result.score > 0 && turn === "A" || result.score < 0 && turn === "B") {
        return best;
      }
    }
  }
  return best;
}

function drawSureWinHands() {
  let sureWinPile = document.getElementById("sure_win_pile");
  while (sureWinPile.lastChild) sureWinPile.lastChild.remove();
  let best = think();
  let tempTurn = turn;
  while (best.moves.length !== 0) {
    let c = document.createElement("div");
    c.innerHTML = tempTurn + ": ";
    tempTurn = _toggleTurn(tempTurn);
    let currentMove = best.moves.pop();
    if (currentMove == null) c.innerHTML += "PASS";
    else {
      for (let j = 0; j < currentMove.length; j++) {
        let span = document.createElement("span");
        span.innerHTML = nameCard(currentMove[j]);
        span.style.color = colorCard(currentMove[j]) === Symbol.for("red") ? "red" : "black";
        span.classList.add("span_card");
        c.appendChild(span);
      }
    }
    sureWinPile.appendChild(c);
  }
  let d = document.createElement("div");
  d.style.fontSize = "150%"
  d.innerHTML += _toggleTurn(tempTurn) + " wins!";
  sureWinPile.appendChild(d);
}
function draw() {
  console.log(hand);
  for (let p in hand) {
    let div = document.getElementById("div" + p);
    while (div.lastChild) div.lastChild.remove();
    sortCardsByRank(hand[p]);
    for (let i = 0; i < hand[p].length; i++) {
      let c = document.createElement("div");
      c.dataset.id = JSON.stringify(hand[p][i]);
      c.innerHTML = hand[p][i] == null ? "PASS" : nameCard(hand[p][i]);
      c.style.color = colorCard(hand[p][i]) === Symbol.for("red") ? "red" : "black";
      c.addEventListener("click", function() {
        if (this.parentNode.id === "div" + turn) {
          this.classList.toggle("chosen");
        }
      });
      div.appendChild(c);
    }
  }
  let pile = document.getElementById("pile");
  while (pile.lastChild) pile.lastChild.remove();
  for (let i = 0; i < stack.length; i++) {
    let c = document.createElement("div");
    if (stack[i] == null) c.innerHTML = "PASS";
    else {
      for (let j = 0; j < stack[i].length; j++) {
        let span = document.createElement("span");
        span.innerHTML = nameCard(stack[i][j]);
        span.style.color = colorCard(stack[i][j]) === Symbol.for("red") ? "red" : "black";
        span.classList.add("span_card");
        c.appendChild(span);
      }
    }
    pile.appendChild(c);
  }
  let sureWinPile = document.getElementById("sure_win_pile");
  while (sureWinPile.lastChild) sureWinPile.lastChild.remove();
  if (_isFinished()) {
    document.getElementById("turn").innerHTML = "Player " + (hand.A.length == 0 ? "A" : "B") + " wins!";
    document.getElementById("winning").innerHTML = "&nbsp;";
  } else {
    document.getElementById("turn").innerHTML = "Player " + turn + "'s turn";
    document.getElementById("winning").innerHTML = "Player " + (think().score > 0 ? "A" : "B") + " can force a win";
  }
  //console.log(counting);
}

function startNewGame() {
  hand = randomCardsGenerator();
  turn = "A";
  stack = [];
  draw();
}
