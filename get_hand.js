function _getSingles(arr) {
  let result = [];
  for (let i = 0; i < arr.length; i++)
    result.push([arr[i]]);
  return result;
}
function _getPairs(arr) {
  let result = [];
  for (let i = 0; i < arr.length; i++) {
    for (let j = i + 1; j < arr.length; j++) {
      if (arr[i].rank === arr[j].rank)
        result.push(sortCardsByRank([arr[i], arr[j]]));
    }
  }
  return result;
}
function _getThreeOfAKinds(arr) {
  let result = [];
  for (let i = 0; i < arr.length - 2; i++) {
    for (let j = i + 1; j < arr.length - 1; j++) {
      for (let k = j + 1; k < arr.length; k++) {
        let combo = [arr[i], arr[j], arr[k]];
        if (_isRankSame(combo))
          result.push(sortCardsByRank(combo));
      }
    }
  }
  return result;
}
function _getFourOfAKinds(arr) {
  let result = [];
  let sortedArr = sortCardsByRank(arr);
  let i = 0;
  while (i < sortedArr.length - 3) {
    let combo = sortedArr.slice(i,i+4);
    if (_isRankSame(combo))
      result.push(combo);
      i += 4;
  }
  return result;
}
function _getStraight(arr) {
  let result = [];
  let sortedArr = sortCardsByRank(arr);
  let rankCount = Array(13).fill(0);
  for (let i = 0; i < sortedArr.length; i++)
    rankCount[sortedArr[i].rank - 3]++;

  for (let i = 0; i < sortedArr.length - 4; i++) {
    let tempCount = rankCount.slice(sortedArr[i].rank-3, sortedArr[i].rank+2);
    if (tempCount.length < 5) break;
    if (tempCount.some(x=>!x)) continue;
    let tempAccCount = [];
    var _accumulator = i;
    for (let p = 0; p < 5; p++) {
      _accumulator += tempCount[p];
      tempAccCount.push(_accumulator);
    }
    for (let j = tempAccCount[0]; j < tempAccCount[1]; j++) {
      for (let k = tempAccCount[1]; k < tempAccCount[2]; k++) {
        for (let m = tempAccCount[2]; m < tempAccCount[3]; m++) {
          for (let n = tempAccCount[3]; n < tempAccCount[4]; n++) {
            let resultArr = [sortedArr[i], sortedArr[j], sortedArr[k], sortedArr[m], sortedArr[n]];
            if (!_isSuitSame(resultArr))
              result.push(resultArr);
          }
        }
      }
    }
    rankCount[sortedArr[i].rank - 3]--;
  }
  return result;
}
function _getFlush(arr) {
  let result = [];
  let sortedArr = sortCardsBySuit(arr);
  let suitCount = Array(4).fill(0);
  for (let i = 0; i < sortedArr.length; i++)
    suitCount[sortedArr[i].suit]++;
  let counter = 0;
  for (let i = 0; i < suitCount.length; i++) {
    if (suitCount[i] < 5) {
      counter += suitCount[i];
      break;
    }
    _combinationUtil(sortedArr, result, [], counter, counter+suitCount[i]-1, 0, 5);
    counter += suitCount[i];
  }
  return result;
}
function _getFullHouse(arr) {
  let result = [];
  let threeOfAKinds = _getThreeOfAKinds(arr);
  let pairs = _getPairs(arr);
  for (let i = 0; i < threeOfAKinds.length; i++) {
    for (let j = 0; j < pairs.length; j++) {
      if (threeOfAKinds[i][0].rank === pairs[j][0].rank)
        continue;
      result.push(sortCardsByRank([...threeOfAKinds[i], ...pairs[j]]));
    }
  }
  return result;
}
function _getFourOfAKindsExtra(arr) {
  let result = [];
  let fourOfAKinds = _getFourOfAKinds(arr);
  let singles = _getSingles(arr);
  for (let i = 0; i < fourOfAKinds.length; i++) {
    for (let j = 0; j < singles.length; j++) {
      if (fourOfAKinds[i][0].rank === singles[j][0].rank)
        continue;
      result.push(sortCardsByRank([...fourOfAKinds[i], ...singles[j]]));
    }
  }
  return result;
}
function _getStraightFlush(arr) {
  let result = [];
  let sortedArr = sortCardsBySuit(arr);
  for (let i = 0; i < sortedArr.length - 4; i++) {
    let combo = sortedArr.slice(i, i+5);
    if (_isRankConsecutive(combo) && _isSuitSame(combo))
      result.push(combo);
  }
  return result;
}
function _compareSingles(arr1, arr2) {
  if (!_isSingle(arr1) || !_isSingle(arr2)) return 0;
  if (arr1[0].rank < arr2[0].rank || arr1[0].rank == arr2[0].rank && arr1[0].suit < arr2[0].suit) return -1;
  return 1;
}
function _comparePairs(arr1, arr2) {
  if (!_isPair(arr1) || !_isPair(arr2)) return 0;
  if (arr1[1].rank < arr2[1].rank || arr1[1].rank == arr2[1].rank && arr1[1].suit < arr2[1].suit) return -1;
  return 1;
}
function _compareThreeOfAKinds(arr1, arr2) {
  if (!_isThreeOfAKind(arr1) || !_isThreeOfAKind(arr2)) return 0;
  if (arr1[2].rank < arr2[2].rank) return -1;
  return 1;
}
function _compareFourOfAKinds(arr1, arr2) {
  if (!_isFourOfAKind(arr1) || !_isFourOfAKind(arr2)) return 0;
  if (arr1[3].rank < arr2[3].rank) return -1;
  return 1;
}
function _compareFiveCards(arr1, arr2) {
  let grade = [0, 0];
  let arr = [arr1, arr2];
  for (let i = 0; i < 2; i++) {
    if (_isStraight(arr[i])) grade[i] = 1;
    if (_isFlush(arr[i])) grade[i] = 2;
    if (_isFullHouse(arr[i])) grade[i] = 3;
    if (_isFourOfAKindExtra(arr[i])) grade[i] = 4;
    if (_isStraightFlush(arr[i])) grade[i] = 5;
    if (grade[i] == 0) return 0;
  }
  if (grade[0] < grade[1]) return -1;
  if (grade[0] > grade[1]) return 1;
  switch (grade[0]) {
    case 1: return _compareSingles(arr1[4], arr2[4]);
    case 2: return _compareSingles(arr1[4], arr2[4]);
    case 3: let c1 = _isThreeOfAKind(arr1.slice(0,3)) ? arr1.slice(0,3) : arr1.slice(2,5);
            let c2 = _isThreeOfAKind(arr2.slice(0,3)) ? arr2.slice(0,3) : arr2.slice(2,5);
            return _compareThreeOfAKinds(c1, c2);
    case 4: let d1 = _isFourOfAKind(arr1.slice(0,4)) ? arr1.slice(0,4) : arr1.slice(1,5);
            let d2 = _isFourOfAKind(arr2.slice(0,4)) ? arr2.slice(0,4) : arr2.slice(1,5);
            return _compareThreeOfAKinds(d1, d2);
    case 5: return _compareSingles(arr1[4], arr2[4]);
    default: return 0;
  }
}

function _isSingle(arr) {
  return arr.length == 1;
}
function _isPair(arr) {
  return arr.length == 2 && _isRankSame(arr);
}
function _isThreeOfAKind(arr) {
  return arr.length == 3 && _isRankSame(arr);
}
function _isFourOfAKind(arr) {
  return arr.length == 4 && _isRankSame(arr);
}
function _isStraight(arr) {
  return arr.length == 5 && _isRankConsecutive(arr);
}
function _isFlush(arr) {
  return arr.length == 5 && _isSuitSame(arr);
}
function _isFullHouse(arr) {
  return arr.length == 5 && ((_isPair(arr.slice(0,2)) && _isThreeOfAKind(arr.slice(2,5)))
  || (_isThreeOfAKind(arr.slice(0,3)) && (_isPair(arr.slice(3,5)))));
}
function _isFourOfAKindExtra(arr) {
  return arr.length == 5 && (_isFourOfAKind(arr.slice(0,4)) || _isFourOfAKind(arr.slice(1,5)));
}
function _isStraightFlush(arr) {
  return arr.length == 5 && _isRankConsecutive(arr) && _isSuitSame(arr);
}

function _isRankConsecutive(arr) {
  for (let i = 0; i < arr.length - 1; i++) {
    if (arr[i+1].rank-arr[i].rank !== 1) return false;
  }
  return true;
}
function _isRankSame(arr) {
  return arr.every(x=>x.rank===arr[0].rank);
}
function _isSuitSame(arr) {
  return arr.every(x=>x.suit===arr[0].suit);
}
function _combinationUtil(arr, combinResult, combin, startOfArr, endOfArr, index, r) {
  if (index === r) {
    if (!_isRankConsecutive(combin))
      combinResult.push(combin.map(x=>x));
    return;
  }
  for (let i = startOfArr; i <= endOfArr && endOfArr-i+1 >= r-index; i++) {
    combin[index] = arr[i];
    _combinationUtil(arr, combinResult, combin, i+1, endOfArr, index+1, r);
  }
}
