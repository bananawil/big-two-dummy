# big-two-dummy

This is a variation of the traditional "Big Two" game with only two players A and B playing alternatively. Each player has 9 cards in their hands from the beginning with all cards facing up, so that both players can always know each other's hand of cards and play accordingly. By drawing out the game tree, there must be a player having a "sure win" strategy.

Game is hosted at http://www.eatmutho.com/big_two_dummy.html.